// Uncomment these imports to begin using these cool features!

import {del, get, post, put, param, requestBody, HttpErrors} from '@loopback/rest';
import {repository} from '@loopback/repository';
import {UserRepository} from '../repositories';
import {User} from '../models';
import * as isemail from 'isemail';
const bcrypt = require('bcrypt');

export class UserController {
  constructor(@repository(UserRepository) private userRepository: UserRepository) {
  }

  @get('/users/{userId}', {
    responses: {
      '200': {
        description: 'Fetch User with id',
        content: {
          'application/json': {
            schema: {
              type: 'User',
            },
          },
        },
      },
    },
  })
  async getUser(@param.path.string('userId') userId: string): Promise<User> {
    const user = await this.userRepository.findById(userId);
    if (!user) {
      throw new HttpErrors.NotFound(`User Not Found with given id ${userId}`);
    }
    return user;
  }

  @get('/users', {
    responses: {
      '200': {
        description: 'Array of Users',
        content: {
          'application/json': {
            schema: {
              type: 'Array', items: {'x-ts-type': User},
            },
          },
        },
      },
    },
  })
  async findAllUsers(): Promise<User[]> {
    return await this.userRepository.find();
  }

  @post('/users', {
    responses: {
      '201': {
        description: 'create a new User',
        content: {
          'application/json': {'x-ts-type': User},
        },
      },
    },
  })
  async createUser(@requestBody() user: User): Promise<User> {
    if (user.id)
      throw new HttpErrors.Conflict(`User id should be null`);

    if (!isemail.validate(user.email))
      throw new HttpErrors.BadRequest('Email Not Valid');

    if (user.password.length < 8)
      throw new HttpErrors.BadRequest('Password Length should not be less than 8 characters');

    user.password = await bcrypt(user.password, 10);
    console.log(user.password);

    return await this.userRepository.create(user);
  }

  @put('/users/{userId}', {
    responses: {
      '200': {
        description: 'Updating a User',
        content: {
          'application/json': {
            schema: {
              type: 'User', items: {'x-ts-type': 'User'},
            },
          },
        },
      },
    },
  })
  async updateUser(@requestBody() user: User, @param.path.string('userId') userId: string): Promise<void> {
    if (user.id !== userId) {
      throw new HttpErrors.BadRequest(`User id ${user.id} and id ${userId} does not match `);
    }
    const user1 = await this.userRepository.findById(userId);
    if (!user1)
      throw new HttpErrors.NotFound(`User doesn't exist with id ${userId}`);

    await this.userRepository.updateById(userId, user);
  }

  @del('/users/{userId}', {
    responses: {
      '204': {
        description: 'Deleting a User',
        content: {
          'application/json': {
            schema: {
              type: 'void',
            },
          },
        },
      },
    },
  })
  async removeUser(@param.path.string('userId') userId: string): Promise<void> {
    const user1 = await this.userRepository.findById(userId);
    if (!user1)
      throw new HttpErrors.NotFound(`User doesn't exist with id ${userId}`);

    return this.userRepository.delete(user1);
  }

}
