/*
import {Strategy} from 'passport';
import {Provider, inject, ValueOrPromise} from '@loopback/context';
import {AuthenticationMetadata, AuthenticationBindings, UserProfile} from '@loopback/authentication';
import {BasicStrategy} from 'passport-http';
import {UserRepository} from '../repositories';
import {repository} from '@loopback/repository';


export class MyAuthStrategyProvider implements Provider<Strategy | undefined> {

  constructor(@inject(AuthenticationBindings.METADATA)
              private metadata: AuthenticationMetadata, @inject(AuthenticationBindings.CURRENT_USER) private user_auth: UserProfile, @repository(UserRepository) private userRepository: UserRepository) {
  }

  value(): ValueOrPromise<Strategy | undefined> {
    if (!this.metadata) {
      return undefined;

      const name = this.metadata.strategy;
      if (name === 'BasicStrategy') {
        return new BasicStrategy(this.verify);
      } else {
        return Promise.reject(`The strategy ${name} is not available.`);
      }
    }
  }

  async verify(
    username: string,
    password: string,
    cb: (err: Error | null, user?: UserProfile | false) => void,
  ) {
    // find user by name & password
    // call cb(null, false) when user not found
    // call cb(null, user) when user is authenticated

    const user1 = this.userRepository.findOne({where: {email: username}});
    if (!user1) {
      cb(null, false);
    } else {
      cb(null, this.user_auth);
    }

  }


}*/
