// Uncomment these imports to begin using these cool features!

import {inject} from '@loopback/context';
import {post, get, requestBody, HttpErrors} from '@loopback/rest';
import {repository} from '@loopback/repository';

//import {AuthenticationBindings, UserProfile, authenticate} from '@loopback/authentication';
import {UserRepository} from '../repositories';


const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

interface Credentials {
  email: string,
  password: string
}

export class AuthenticationController {
  constructor(@repository(UserRepository) private userRepo: UserRepository) {
  }

  /*
    @authenticate('BasicStrategy')
    @get('/whoami')
    whoami(): string {
      return this.user.id;
    }
  */

  @post('/users/login')
  async login(@requestBody() credentials: Credentials): Promise<object> {

    if (credentials == null) {
      throw new HttpErrors.BadRequest('Credentials Request is null');
    }

    let searchQuery: any = {
      where: {
        email: credentials.email,
      },
    };

    const user = await this.userRepo.findOne(searchQuery);
    if (!user) {
      throw new HttpErrors.NotFound('Username or password is wrong');
    } else {
      if (bcrypt.compareSync(credentials.password, user.password)) {
        // Passwords match
        const token = jwt.sign({
          data: user.email,
        }, 'secret', {expiresIn: '1h'});

        return {
          user: user,
          token: token,
          status:200
        };
      } else {
        // Passwords don't match
        throw new HttpErrors.Unauthorized('Username or password is wrong');
      }
    }
  }

}
